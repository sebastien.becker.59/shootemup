﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{

    private WeaponScript[] weapons;

    void Awake()
    {
        weapons = GetComponentsInChildren<WeaponScript>();
    }

    // Update is called once per frame
    void Update()
    {
        foreach (var weapon in weapons)
            if (weapon.CanAttack)
            {
                weapon.Attack(true);
                SoundEffectsHelper.Instance.MakeEnemyShotSound();
            }
    }
}
