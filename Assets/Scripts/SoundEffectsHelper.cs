﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectsHelper : MonoBehaviour
{
    public static SoundEffectsHelper Instance;

    public AudioClip explosionSound;
    public AudioClip playShotSound;
    public AudioClip enemyShotSound;

    void Awake()
    {
        if(Instance != null)
        {
            Debug.LogError("Multiple Instance of SoundEffectsHelper");
        }
        Instance = this;
    }

    private void MakeSound(AudioClip originalClip)
    {
        AudioSource.PlayClipAtPoint(originalClip, transform.position);
    }

    public void MakeExplosionSound()
    {
        MakeSound(explosionSound);
    }

    public void MakePlayerShotSound()
    {
        MakeSound(playShotSound);
    }

    public void MakeEnemyShotSound()
    {
        MakeSound(enemyShotSound);
    }
}
