﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Player controller and behavior
/// </summary>
public class PlayerScript : MonoBehaviour
{
    /// <summary>
    /// 1 - The speed of the ship
    /// </summary>
    public Vector2 speed = new Vector2(50, 50);

    /// <summary>
    /// 2 - Store the movement and the component
    /// </summary>
    private Vector2 movement;
    private Rigidbody2D rigidbody;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

        float inputX = Input.GetAxis("Horizontal");
        float inputY = Input.GetAxis("Vertical");

        movement = new Vector2(speed.x * inputX, speed.y * inputY);
        rigidbody.velocity = movement;

        bool shoot = Input.GetButtonDown("Fire1");
        if(shoot)
        {
            WeaponScript weapon = GetComponent<WeaponScript>();
            weapon.Attack(false);
            SoundEffectsHelper.Instance.MakePlayerShotSound();
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        bool damagePlayer = false;
        EnemyScript enemy = collision.gameObject.GetComponent<EnemyScript>();
        if(enemy != null) {
            HealthScript enemyHealth = enemy.GetComponent<HealthScript>();
            enemyHealth.Damage(enemyHealth.hp);
            damagePlayer = true;
        }

        if(damagePlayer)
        {
            HealthScript playerHealth = GetComponent<HealthScript>();
            playerHealth.Damage(1);
        }
    }

}
